<div class="genericSection section-ISOTOPE-PORTFOLIO">
        
    <!--add here url for this section background image-->
    <img class="sectionBackgroundImage" src="pages/backgrounds/background_gears4.jpg" />
    <!--/section background image-->
    
    <!--add here page number-->
    <p class="pageNum textColor01">02/<span class="textColor03">02</span></p>
    <!--/add here page number-->
    
    <!--skeleton container-->
    <div class="container">
                
        <!--Website logo-->
        <div class="row">
            <div class="sixteen columns">
                <img class="brand" src="images/logo.png" alt="logo" />
            </div>
        </div>
        <!--/Website logo-->
        
        
        <!--Section title H1-->
        <div class="row">
            <div class="sixteen columns">
                <div class="h2square backgroundColor02"></div>
                <h2 class="textColor01">Past Events<span class="textColor02"> // What We Do</span></h2>
                <div class="horizontalLine backgroundColor02"></div>
                <div class="clear-fx"></div>
            </div>
        </div>
        <!--/Section title H1-->
        
        <!--full width text-->
        <div class="row">
            <div class="sixteen columns">
                <ul class="isotopeMenu">
                    <li><a class="iconAll" href="*">All</a></li>
                    <li><a class="wheel" href="pro">Professionalism</a></li>
                    <li><a class="wheel" href="bro">Brotherhood</a></li>
                    <li><a class="wheel" href="com">Cummunity</a></li>
                </ul>
            </div>         
        </div>
        <!--/full width text-->        
        
        <!--isotope gallery-->
        <div class="row">
            <div class="sixteen columns">
                <div class="isotopeContainer">
                    
                    <!--item without caption-->
                    <div class="cat1 isotopeItem imageItemType captionHoverItem imageSize1">
                        <a class="whiteHover" data-lightbox="photo-1" title="check 1 2" href="pages/portfolio_images/1.jpg">LightBoxCheck</a>                   
                    </div>
                    <!--/item without caption-->
                    
                    
                    <!--item without caption-->
                    <div class="cat1 isotopeItem imageItemType whiteHoverItem imageSize1">
                        <img src="pages/portfolio_images/3.jpg" />
                        <a class="whiteHover" href="#!SingleProject.html"></a>
                        <div class="plusIco"></div>                                                
                    </div>
                    <!--/item without caption-->                    
                    
                    
                    <!--item with caption-->
                    <div class="cat2 isotopeItem imageItemType captionHoverItem imageSize1">
                        <a href="pages/portfolio_images/1.jpg" data-lightbox="photo-1" title="check 1 2"><img src="pages/portfolio_images/2.jpg" /></a>
                        <div class="whiteCaption">
                            <div class="whiteCaptionBackground"></div>                          
                            <p class="isotopeCaptionText textColor04">Testing!</p>         
                        </div>
                        <div class="plusIco"></div>                       
                    </div>
                    <!--/item with caption-->
                    
                                       
                    
                    <!--text item-->
                    <div class="cat3 isotopeItem isotomeTextItem textSize2">
                        <div class="isotopeHeaderText backgroundColor02">
                            <p class="textColor01">Project2</p>
                        </div>
                        <p class="defaultText textColor01">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rutrum tempor facilisis. Proin purus est.Lorem ipsum dolor sit amet, consectur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectur adipiscing elit.
                        </p>
                    </div>
                    <!--text item-->
                    
                    <!--item without caption-->
                    <div class="cat1 isotopeItem imageItemType whiteHoverItem imageSize1">
                        <img src="pages/portfolio_images/6.jpg" />
                        <a class="whiteHover" href="#!SingleProject.html"></a>
                        <div class="plusIco"></div>                                                
                    </div>
                    <!--/item without caption-->                    
                    
                    
                    <!--item with caption-->
                    <div class="cat2 isotopeItem imageItemType captionHoverItem imageSize1">
                        <a href="#!SingleProject.html"><img src="pages/portfolio_images/5.jpg" /></a>
                        <div class="whiteCaption">
                            <div class="whiteCaptionBackground"></div>                          
                            <p class="isotopeCaptionText textColor04">projects</p>                            
                        </div>
                        <div class="plusIco"></div>                       
                    </div>
                    <!--/item with caption--> 
                    
                    
                    <!--item without caption-->
                    <div class="cat1 isotopeItem imageItemType whiteHoverItem imageSize2">
                        <img src="pages/portfolio_images/4.jpg" />
                        <a class="whiteHover" href="#!SingleProject.html"></a>
                        <div class="plusIco"></div>                                                
                    </div>
                    <!--/item without caption-->
                                                                                                    
            
                    <!--text item-->
                    <div class="cat3 isotopeItem isotomeTextItem textSize1">
                        <div class="isotopeHeaderText backgroundColor02">
                            <p class="textColor01">Project1</p>
                        </div>
                        <p class="defaultText textColor01">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rutrum tempor facilisis. Proin purus est.Lorem ipsum dolor sit amet, consectur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectur adipiscing elit. Proin purus est.Lorem ipsum dolor sit amet, consectur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectur adipiscing elit.
                        </p>
                    </div>
                    <!--text item-->
                                                             
                                     
                
                                        
                </div>
            </div>
        </div>
        <!--/isotope gallery-->    
        

    </div>
    <!--/skeleton container-->
 
    
         
</div>